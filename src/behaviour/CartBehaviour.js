import React from 'react';

function cartBehaviour(store_id, action, cartTree, amount = 1){
    if(cartTree){
        
        if(action === "ADD"){
            if(!cartTree[store_id]){
                cartTree[store_id] = amount;
            }
            else{
                let c = cartTree[store_id] + amount;
                cartTree[store_id]  = c;
            }
        }
        else{
            if(!cartTree[store_id] || cartTree[store_id] === 0){
                return;
            }
            else{
                let c = cartTree[store_id] - amount;
                cartTree[store_id]  = c;
            }
        }
    }

    return cartTree;
}


export default cartBehaviour;