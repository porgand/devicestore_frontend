function CloseCrossButton(props){
    return(
        <button type="button" 
                className="close" 
                aria-label="Close" 
                onClick={props.handleClose}>
            <span aria-hidden="true">&times;</span>
        </button>
    );
}

export default CloseCrossButton;